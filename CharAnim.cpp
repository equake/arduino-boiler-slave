#include <Arduino.h>
#include "CharAnim.h";

CharAnim::CharAnim(char* frames, short ms) {
  CharAnim::frames = frames;
  CharAnim::ms = ms;
}

bool CharAnim::isFrameReady() {
  if (millis() - CharAnim::lastDraw > CharAnim::ms) {
    return true;
  } else {
    return false;
  }
}

char CharAnim::getNextFrame() {
  char frame = frames[CharAnim::frameNum];
  if (frame == '\0' || frameNum > 30) {
    CharAnim::frameNum = 0;
    frame = frames[CharAnim::frameNum];
  }
  CharAnim::frameNum++;
  CharAnim::lastDraw = millis();
  return frame;
}
