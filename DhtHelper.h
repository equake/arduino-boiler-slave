class DhtHelper {
  DHT dht;

  String status;
  float humidity;
  float temperature;
  
  long lastQuery = 0;
  
  void getSensorData();
  
  public:
    DhtHelper(DHT d);
    String getStatus();
    float getHumidity();
    float getTemperature();
};
