#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <BigNumbersI2C.h>
#include <DHT.h>
#include <SPI.h>
#include <RH_NRF24.h>
#include <EEPROM.h>

#include "CharAnim.h";

DHT dht;
DHT dht2;

RH_NRF24 nrf24;

byte lastTemperature;
byte temperature;

long now = 0;
long lastTempChange = 0;

short avgBtn = 0;

char fanAnimFrames[5] = {0x7C, 0x2F, 0x2D, 0xCD};
char fireAnimFrames[9] = {0xC6, 0xAF, 0xBF, 0xB3, 0xC3, 0xD9, 0xD7, 0xDE};
char waterAnimFrames[5] = {0xDE, 0xEB, 0xA5, '.'};

CharAnim fanAnim(fanAnimFrames, 150);
CharAnim fireAnim(fireAnimFrames, 50);
CharAnim waterAnim(waterAnimFrames, 300);

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7); // 0x27 is the I2C bus address for an unmodified module
BigNumbers bigNum(&lcd); // construct BigNumbers object, passing to it the name of our LCD object

void setup() {
  Serial.begin(9600);
  
  lastTemperature = temperature = EEPROM.read(0);
    
  dht.setup(A3); // data pin Analog 3
  dht2.setup(4); // data pin Digital 4
  
  if (nrf24.init()) {
    Serial.println("Radio initialized!");
  } else {
    Serial.println("Error initializing radio :(");
  }
  if (!nrf24.setChannel(1))
    Serial.println("setChannel failed");
  if (!nrf24.setRF(RH_NRF24::DataRate250kbps, RH_NRF24::TransmitPower0dBm))
    Serial.println("setRF failed");    
  
  pinMode(A0, INPUT_PULLUP); // sets analog pin for input

  lcd.begin(16, 2); // setup LCD rows and columns
  lcd.setBacklightPin(3, POSITIVE);
  lcd.setBacklight(HIGH);
  bigNum.begin(); // set up BigNumbers
  lcd.clear(); // clear display
  
  analogWrite(6, 60);

  drawTemperature(temperature);

  lcd.setCursor(13, 0);
  lcd.print("W");

  lcd.setCursor(14, 0);
  lcd.print("E");

  lcd.setCursor(15, 0);
  lcd.print("F");

  //  lcd.setCursor(7,0);
  //  lcd.print("W F G");
  now = lastTempChange = millis();

}

void loop()
{  
  byte buttonPressed = readButton(A0);
  if (buttonPressed > 0) {
    tone(2, 1000, 20);
    if (buttonPressed == 1) {
      temperature++;
    } else if (buttonPressed == 2) {
      temperature--;
    } else if (buttonPressed == 3) {
      temperature = 20;
    }
    if (temperature < 20) {
      temperature = 20;
    } else if (temperature > 60) {
      temperature = 60;
    }
    drawTemperature(temperature);
    lastTempChange = now;
  }
  if (temperature != lastTemperature && now - lastTempChange > 500) {
    lastTemperature = temperature;
    setTemperature(temperature);
  }
  
  setLCDBackLight(A1);

  drawStatus();

  float temp = getDHTTemp();
  if (temp > 0) {
    lcd.setCursor(7,0);
    lcd.print(temp);
  }
  
  float temp2 = getDHTTemp2();
  if (temp2 > 0) {
    lcd.setCursor(7,1);
    lcd.print(temp2);
  }
  
  now = millis();
}

byte lastDrawnT = 0;
void drawTemperature(byte t) {
  if (lastDrawnT != t) {
    lastDrawnT = t;
    bigNum.displayLargeInt(t, 0, 2, true);
    lcd.setCursor(6, 0);
    lcd.print((char)0xDF);
  }
}

void setTemperature(byte t) {
  Serial.print("Setting temperature to ");
  Serial.print(t);
  Serial.println("c");
  tone(2, 2400);
  delay(20);
  tone(2, 4800, 20);
  byte buf[2] = {'0x01', t};
  if (nrf24.send(buf, 2)) {
    Serial.println("Temperature set on boiler!");
  } else {
    Serial.println("Failed to transmit temperature");
  }
  EEPROM.write(0, t);
  drawTemperature(t);
}

float lastDHTTemp = 0;
long lastDHTReadTime = 0;
float getDHTTemp() {
  if (now - lastDHTReadTime > dht.getMinimumSamplingPeriod()) {
    float humidity = dht.getHumidity();
    float temperature = dht.getTemperature();
  
    Serial.print("1\t");
    Serial.print(dht.getStatusString());
    Serial.print("\t");
    Serial.print(humidity, 1);
    Serial.print("\t\t");
    Serial.print(temperature, 1);
    Serial.print("\t\t");
    Serial.println(dht.toFahrenheit(temperature), 1);
    lastDHTTemp = temperature;
    lastDHTReadTime = now;
  }
  return lastDHTTemp;
}

float lastDHTTemp2 = 0;
long lastDHTReadTime2 = 0;
float getDHTTemp2() {
  if (now - lastDHTReadTime2 > dht2.getMinimumSamplingPeriod()) {
    float humidity = dht2.getHumidity();
    float temperature = dht2.getTemperature();
    
    Serial.print("2\t");
    Serial.print(dht2.getStatusString());
    Serial.print("\t");
    Serial.print(humidity, 1);
    Serial.print("\t\t");
    Serial.print(temperature, 1);
    Serial.print("\t\t");
    Serial.println(dht2.toFahrenheit(temperature), 1);
    lastDHTTemp2 = temperature;
    lastDHTReadTime2 = now;
  }
  return lastDHTTemp2;
}

long lastButtonEventTime = 0;
long lastButtonRepeat = 0;
byte lastButtonState = 0;
byte readButton(short pin) {
  byte button = 0;
  short pinValue = analogRead(pin);  
  if (pinValue >= 200 && pinValue <= 250) {
    button = 1;
  } else if (pinValue >= 330 && pinValue <= 380) {
    button = 2;
  } else if (pinValue >= 420 && pinValue <= 480) {
    button = 3;
  }
  if (button != 0 && lastButtonState == 0) { // press
    lastButtonEventTime = lastButtonRepeat = now;
    lastButtonState = button;
    button = 0;
  } else if (button != 0 && now - lastButtonEventTime >= 250 && now - lastButtonRepeat >= 66) { // repeat
    lastButtonRepeat = now;
    button = lastButtonState;
  } else if (button == 0 && lastButtonState != 0) { // release
    button = lastButtonState;
    lastButtonState = 0;
    if (lastButtonRepeat > lastButtonEventTime) {
      button = 0;
    }
  } else {
    button = 0;
  }
  return button;
}

void drawStatus() {
  if (waterAnim.isFrameReady()) {
    lcd.setCursor(13, 1);
//    if (waterOn) {
      lcd.print(waterAnim.getNextFrame());
//    } else {
//      lcd.print(' ');
//    }
  }
  if (fanAnim.isFrameReady()) {
    lcd.setCursor(14, 1);
    lcd.print(fanAnim.getNextFrame());
  }
  if (fireAnim.isFrameReady()) {
    lcd.setCursor(15, 1);
    lcd.print(fireAnim.getNextFrame());
  }
}

void setLCDBackLight(short pin) {
    short lum = analogRead(pin)/4;
    analogWrite(6, lum);
}
