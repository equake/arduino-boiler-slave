#include <DHT.h>
#include "DhtHelper.h"

DhtHelper::DhtHelper(DHT d) {
  DhtHelper::dht = d;
}

void DhtHelper::getSensorData() {
  if (millis() - DhtHelper::lastQuery >= DhtHelper::dht.getMinimumSamplingPeriod()) {
    DhtHelper::status = DhtHelper::dht.getStatusString();
    DhtHelper::humidity = DhtHelper::dht.getHumidity();
    DhtHelper::temperature = DhtHelper::dht.getTemperature();
  }
}

String DhtHelper::getStatus() {
  DhtHelper::getSensorData();
  return DhtHelper::status;
}

float DhtHelper::getHumidity() {
  DhtHelper::getSensorData();
  return DhtHelper::humidity;
}

float DhtHelper::getTemperature() {
  DhtHelper::getSensorData();
  return DhtHelper::temperature;
}
