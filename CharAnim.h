class CharAnim {

  char* frames; // chars to cycle
  short ms; // time between frames
  short frameNum = 0;
  long lastDraw = 0; // last frame rendering timestamp 

  public:
    CharAnim(char* frames, short ms);
    bool isFrameReady();
    char getNextFrame();

};
